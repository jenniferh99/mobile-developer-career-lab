package com.example.spotifyreimagined2.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PlaySongViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public PlaySongViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is play song fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}