package com.example.spotifyreimagined2.ui.playlist;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PodcastListViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public PodcastListViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is podcast list fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}