package com.example.spotifyreimagined2.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PlayPodcastViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public PlayPodcastViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is play podcast fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}