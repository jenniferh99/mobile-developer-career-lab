package com.example.spotifyreimagined2.ui.playlist;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.spotifyreimagined2.PlaylistAdapter;
import com.example.spotifyreimagined2.R;
import com.example.spotifyreimagined2.databinding.FragmentPlaylistBinding;

public class PlaylistFragment extends Fragment {
    private PlaylistViewModel playlistViewModel;
    private FragmentPlaylistBinding binding;
    private PlaylistAdapter playlistAdapter;
    protected RecyclerView recyclerView;
    private enum LayoutManagerType {
        LINEAR_LAYOUT_MANAGER
    }
    protected RecyclerView.LayoutManager layoutManager;
    protected LayoutManagerType currentLayoutManagerType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        playlistViewModel = new PlaylistViewModel();
        new ViewModelProvider(this).get(PlaylistViewModel.class);

        binding = FragmentPlaylistBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textPlaylist;

        playlistViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        String[] data = new String[20];
        this.playlistAdapter = new PlaylistAdapter(data);
        this.recyclerView = (RecyclerView) root.findViewById(R.id.playlistRecyclerView);
        this.recyclerView.setAdapter(this.playlistAdapter);
        initDataset();

        return root;
    }

    public void setRecyclerViewLayoutManager() {
        int scrollPosition = 0;

        // If a layout manager has already been set, get current scroll position.
        if (recyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) recyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }

        layoutManager = new LinearLayoutManager(getActivity());
        currentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.scrollToPosition(scrollPosition);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    public void initDataset(){
        String[] mLinearData = new String[20];
        for (int i = 0; i < 20; i++) {
            mLinearData[i] = "Some Song Name";
        }
        this.playlistAdapter.setLinearData(mLinearData);
    }
}

