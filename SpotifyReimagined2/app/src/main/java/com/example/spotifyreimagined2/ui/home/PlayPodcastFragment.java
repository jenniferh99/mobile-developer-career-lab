package com.example.spotifyreimagined2.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.spotifyreimagined2.databinding.FragmentPlayPodcastBinding;
import com.example.spotifyreimagined2.databinding.FragmentPlaySongBinding;

public class PlayPodcastFragment extends Fragment {

    private PlayPodcastViewModel playPodcastViewModel;
    private FragmentPlayPodcastBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        playPodcastViewModel =
                new ViewModelProvider(this).get(PlayPodcastViewModel.class);

        binding = FragmentPlayPodcastBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textPlayPodcast;
        playPodcastViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}